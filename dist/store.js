"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.setStore = exports.keyStores = exports.originalKeyStores = void 0;
const store = new Map();
exports.originalKeyStores = new Map();
exports.keyStores = new Map();
exports.setStore = (target, options) => {
    const { key, ...rest } = options;
    const storeKey = target.constructor;
    const targetStore = store.has(storeKey) ? store.get(storeKey) : new Map();
    let value = targetStore.has(key) ? targetStore.get(key) : {};
    value = {
        ...value,
        ...rest,
    };
    targetStore.set(key, value);
    store.set(storeKey, targetStore);
};
exports.default = store;
