"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getKeyStore = exports.getOriginalKeyStore = exports.isNullOrUndefined = exports.isUndefined = exports.isNull = void 0;
const store_1 = __importStar(require("./store"));
exports.isNull = (val) => val === null;
exports.isUndefined = (val) => val === undefined;
exports.isNullOrUndefined = (val) => exports.isNull(val) || exports.isUndefined(val);
exports.getOriginalKeyStore = (Clazz) => {
    let curLayer = Clazz;
    const cacheOriginalKeyStore = store_1.originalKeyStores.get(curLayer);
    if (cacheOriginalKeyStore) {
        return cacheOriginalKeyStore;
    }
    const originalKeyStore = new Map();
    while (curLayer.name && curLayer.prototype) {
        const { constructor } = curLayer.prototype;
        const targetStore = store_1.default.get(constructor);
        if (targetStore) {
            targetStore.forEach((storeItem, key) => {
                const item = {
                    key,
                    ...storeItem,
                };
                if (!originalKeyStore.has(storeItem.originalKey)) {
                    originalKeyStore.set(storeItem.originalKey, [item]);
                }
                else {
                    const exists = originalKeyStore.get(storeItem.originalKey);
                    if (!exists.find((exist) => exist.key === key)) {
                        originalKeyStore.set(storeItem.originalKey, [...originalKeyStore.get(storeItem.originalKey), item]);
                    }
                }
            });
        }
        curLayer = Object.getPrototypeOf(constructor);
    }
    store_1.originalKeyStores.set(Clazz, originalKeyStore);
    return originalKeyStore;
};
exports.getKeyStore = (Clazz) => {
    const cacheKeyStore = store_1.keyStores.get(Clazz);
    if (cacheKeyStore) {
        return cacheKeyStore;
    }
    const keyStore = new Map();
    const originalKeyStore = exports.getOriginalKeyStore(Clazz);
    originalKeyStore.forEach(storeItems => {
        const [firstStoreItem] = storeItems;
        if (storeItems.length === 1) {
            keyStore.set(firstStoreItem.key, firstStoreItem);
        }
        else {
            const hasStoreItems = storeItems.filter(storeItem => storeItem.serializeTarget);
            if (hasStoreItems.length !== 1) {
                throw new Error(`Only one of keys(${storeItems.map(storeItem => storeItem.key).join(', ')}) in ${Clazz.name} can contain a serializeTarget when use toPlain`);
            }
            const hasStoreItem = hasStoreItems[0];
            keyStore.set(hasStoreItem.key, hasStoreItem);
        }
    });
    store_1.keyStores.set(Clazz, keyStore);
    return keyStore;
};
