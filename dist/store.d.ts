import { StoreItemOptions, StoreItemType } from './typing';
declare const store: Map<Function, Map<string, StoreItemType>>;
export declare const originalKeyStores: Map<Function, Map<string, StoreItemOptions[]>>;
export declare const keyStores: Map<Function, Map<string, StoreItemType>>;
export declare const setStore: (target: Function, options: StoreItemOptions) => void;
export default store;
