import { JosnType, BasicClass, ToClassOptions } from './typing';
export declare const arrayItemToClass: <T>(arrayVal: any[], Clazz: BasicClass<T>, options: ToClassOptions) => any;
export declare const toClasses: <T>(rawJson: JosnType[], Clazz: BasicClass<T>, options?: ToClassOptions) => T[];
export declare const toClass: <T>(rawJson: JosnType, Clazz: BasicClass<T>, options?: ToClassOptions) => T;
