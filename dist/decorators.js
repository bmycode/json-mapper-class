"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.defaultVal = exports.optional = exports.typed = exports.property = exports.serializeTarget = exports.afterSerialize = exports.beforeDeserialize = exports.deserialize = exports.serialize = void 0;
const store_1 = require("./store");
function serialize(serializer, disallowIgnore = false) {
    return (target, propertyKey) => {
        store_1.setStore(target, {
            key: propertyKey,
            serializer,
            disallowIgnoreSerializer: disallowIgnore,
        });
    };
}
exports.serialize = serialize;
function deserialize(deserializer, disallowIgnore = false) {
    return (target, propertyKey) => {
        store_1.setStore(target, {
            key: propertyKey,
            deserializer,
            disallowIgnoreDeserializer: disallowIgnore,
        });
    };
}
exports.deserialize = deserialize;
function beforeDeserialize(beforeDeserializer, disallowIgnore = false) {
    return (target, propertyKey) => {
        store_1.setStore(target, {
            key: propertyKey,
            beforeDeserializer,
            disallowIgnoreBeforeDeserializer: disallowIgnore,
        });
    };
}
exports.beforeDeserialize = beforeDeserialize;
function afterSerialize(afterSerializer, disallowIgnore = false) {
    return (target, propertyKey) => {
        store_1.setStore(target, {
            key: propertyKey,
            afterSerializer,
            disallowIgnoreAfterSerializer: disallowIgnore,
        });
    };
}
exports.afterSerialize = afterSerialize;
function serializeTarget() {
    return (target, propertyKey) => {
        store_1.setStore(target, {
            key: propertyKey,
            serializeTarget: true,
        });
    };
}
exports.serializeTarget = serializeTarget;
function property(originalKey, targetClass, isOptional = false) {
    return (target, propertyKey) => {
        const config = {
            originalKey: originalKey || propertyKey,
            key: propertyKey,
        };
        if (targetClass) {
            config.targetClass = targetClass.prototype.constructor;
        }
        if (isOptional) {
            config.optional = isOptional;
        }
        store_1.setStore(target, config);
    };
}
exports.property = property;
function typed(targetClass) {
    return (target, propertyKey) => {
        store_1.setStore(target, {
            key: propertyKey,
            targetClass,
        });
    };
}
exports.typed = typed;
function optional() {
    return (target, propertyKey) => {
        store_1.setStore(target, {
            key: propertyKey,
            optional: true,
        });
    };
}
exports.optional = optional;
function defaultVal(value) {
    return (target, propertyKey) => {
        store_1.setStore(target, {
            key: propertyKey,
            default: value,
        });
    };
}
exports.defaultVal = defaultVal;
