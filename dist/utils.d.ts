import { StoreItemOptions, StoreItemType, BasicClass } from './typing';
export declare const isNull: (val: any) => boolean;
export declare const isUndefined: (val: any) => boolean;
export declare const isNullOrUndefined: (val: any) => boolean;
export declare const getOriginalKeyStore: <T>(Clazz: BasicClass<T>) => Map<string, StoreItemOptions[]>;
export declare const getKeyStore: <T>(Clazz: BasicClass<T>) => Map<string, StoreItemType>;
