import { JosnType, BasicClass, ToPlainOptions } from './typing';
export declare const arrayItemToObject: <T>(arrayVal: any[], Clazz: BasicClass<T>, options: ToPlainOptions) => any;
export declare const toPlains: <T>(instances: (JosnType | T)[], Clazz: BasicClass<T>, options?: ToPlainOptions) => any[];
export declare const toPlain: <T>(instance: JosnType | T, Clazz: BasicClass<T>, options?: ToPlainOptions) => any;
