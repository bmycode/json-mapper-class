"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.toClass = exports.toClasses = exports.arrayItemToClass = void 0;
const isarray_1 = __importDefault(require("isarray"));
const utils_1 = require("./utils");
exports.arrayItemToClass = (arrayVal, Clazz, options) => {
    return arrayVal.map((v) => isarray_1.default(v) ? exports.arrayItemToClass(v, Clazz, options) : objectToClass(v, Clazz, options));
};
const objectToClass = (jsonObj, Clazz, options) => {
    const instance = new Clazz();
    const originalKeyStore = utils_1.getOriginalKeyStore(Clazz);
    originalKeyStore.forEach((propertiesOptions, originalKey) => {
        const originalValue = jsonObj[originalKey];
        propertiesOptions.forEach((storeItemoptions) => {
            const { key, beforeDeserializer, deserializer, targetClass, optional } = storeItemoptions;
            const disallowIgnoreDeserializer = storeItemoptions.disallowIgnoreDeserializer || !options.ignoreDeserializer;
            const disallowIgnoreBeforeDeserializer = storeItemoptions.disallowIgnoreBeforeDeserializer || !options.ignoreBeforeDeserializer;
            const isValueNotExist = options.distinguishNullAndUndefined ? utils_1.isUndefined : utils_1.isNullOrUndefined;
            let value = originalValue;
            if (isValueNotExist(value)) {
                if (!isValueNotExist(storeItemoptions.default)) {
                    instance[key] = storeItemoptions.default;
                    return;
                }
                if (!optional) {
                    throw new Error(`Can't map '${originalKey}' to ${Clazz.name}.${key}, property '${originalKey}' not found`);
                }
                return;
            }
            value =
                beforeDeserializer && disallowIgnoreBeforeDeserializer
                    ? beforeDeserializer(value, instance, jsonObj, options)
                    : value;
            if (value && targetClass) {
                if (isarray_1.default(value)) {
                    value = exports.arrayItemToClass(value, targetClass, options);
                }
                else {
                    value = exports.toClass(value, targetClass, options);
                }
            }
            instance[key] =
                deserializer && disallowIgnoreDeserializer ? deserializer(value, instance, jsonObj, options) : value;
        });
    });
    return instance;
};
exports.toClasses = (rawJson, Clazz, options = {}) => {
    if (!isarray_1.default(rawJson)) {
        throw new Error(`rawJson ${rawJson} must be an array`);
    }
    const { constructor } = Clazz.prototype;
    return rawJson.map((item) => objectToClass(item, constructor, options));
};
exports.toClass = (rawJson, Clazz, options = {}) => {
    const { constructor } = Clazz.prototype;
    return objectToClass(rawJson, constructor, options);
};
