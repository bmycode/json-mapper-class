import { StoreItemOptions, StoreItemType } from './typing';

const store = new Map<Function, Map<string, StoreItemType>>();

export const originalKeyStores = new Map<Function, Map<string, StoreItemOptions[]>>();
export const keyStores = new Map<Function, Map<string, StoreItemType>>();

export const setStore = (target: Function, options: StoreItemOptions) => {
  const { key, ...rest } = options;
  // 获取被装饰 属性 所在类的 构造函数，作为存储 Map 的 key
  const storeKey = target.constructor;
  // 判断是否已经存储过，如果已经存在 get 获取之前存在的 key 对应的数值，不存在 创建新的 map
  const targetStore = store.has(storeKey) ? store.get(storeKey) : new Map<string, StoreItemType>();

  // 判断被装饰的 属性（id,name）是否已经存在， 存在 获取 key 对应 的 StoreItemType，不存在为 {}
  let value = targetStore.has(key) ? targetStore.get(key) : {};
  value = {
    ...value,
    ...rest,
  };
  
  targetStore.set(key, value);
  store.set(storeKey, targetStore);
};

export default store;
